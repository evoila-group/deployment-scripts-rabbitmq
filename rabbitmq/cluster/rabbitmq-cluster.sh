#!/bin/bash
echo ""
echo ""
echo "### Starting cluster setup for rabbitmq ###"
echo ""

export PATH=/usr/lib/rabbitmq/bin:$PATH
export HOME=/var/lib/rabbitmq
export DATA_PATH=/data/rabbitmq

export ENVIRONMENT=${ENVIRONMENT}

#path used to check if service is installed
export CHECK_PATH=$DATA_PATH

usage() {
  echo "Error: $1"
  echo "-e: erlang key; -t: host type [primary | secondary]; -m: master host (mandatory for secondary)"
  echo "Usage: $0 [-e <string>] [-t <string>] [-m <string>]"
  1>&2;
  exit 1;
}

while getopts ":e:t:m:d:p:u:" o; do
    case "${o}" in
        e)
            ERLANG_KEY=${OPTARG}
            ;;
        t)
            HOST_TYPE=${OPTARG}
            ;;
        m)
            MASTER=${OPTARG}
            ;;
        d)
            RABBITMQ_DEFAULT_VHOST=${OPTARG}
            ;;
        p)
            RABBITMQ_DEFAULT_PASS=${OPTARG}
            ;;
        u)
            RABBITMQ_DEFAULT_USER=${OPTARG}
            ;;
        *)
            usage "unknown parameter"
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${ERLANG_KEY}" ] || [ -z "${HOST_TYPE}" ]; then
    usage "missing parameter"
fi

if [ "$HOST_TYPE" = 'secondary' ]; then
  if [ -z "${MASTER}" ]; then
      usage "master must be set for secondary hosts"
  fi
fi

echo "Try unmonitor rabbitmq"
while 
    sleep 2
    monit unmonitor rabbitmq-server
    [[ $? != 0 ]]
do
    :
done
monit summary
rabbitmqctl stop_app
systemctl stop rabbitmq-server

echo "Wait for monit stop rabbitmq-server - 15 sec"
sleep 15

echo "Copy ERLANG_KEY to /var/lib/rabbitmq/.erlang.cookie"
echo ${ERLANG_KEY} > /var/lib/rabbitmq/.erlang.cookie

echo '[{rabbit, [{loopback_users, []},{cluster_partition_handling, autoheal}]}].' > /etc/rabbitmq/rabbitmq.config

echo "Restart rabbitmq-server - wait 15 sec"
systemctl start rabbitmq-server
sleep 15
rabbitmqctl start_app


if [ "$HOST_TYPE" = 'secondary' ]; then
  rabbitmqctl stop_app
  echo rabbitmqctl join_cluster rabbit@${MASTER}
  rabbitmqctl join_cluster rabbit@${MASTER}
  rabbitmqctl start_app
fi

monit monitor rabbitmq-server

echo "Cluster Status:"
rabbitmqctl cluster_status


