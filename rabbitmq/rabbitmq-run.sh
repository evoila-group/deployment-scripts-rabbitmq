#!/bin/bash
echo
echo "### Starting rabbitmq-run.sh... ###"
echo

set -e

if [ "$RABBITMQ_ERLANG_COOKIE" ]; then
	cookieFile='/var/lib/rabbitmq/.erlang.cookie'
	if [ -e "$cookieFile" ]; then
		if [ "$(cat "$cookieFile" 2>/dev/null)" != "$RABBITMQ_ERLANG_COOKIE" ]; then
			echo >&2
			echo >&2 "warning: $cookieFile contents do not match RABBITMQ_ERLANG_COOKIE"
			echo >&2
		fi
	else
		echo "$RABBITMQ_ERLANG_COOKIE" > "$cookieFile"
		chmod 600 "$cookieFile"
		chown rabbitmq "$cookieFile"
	fi
fi

# starts rabbitmq-server

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
		systemctl start rabbitmq-server
		sleep 10
		$OPENSTACK_START

		echo "Starting RabbitMQ exproter"
		systemctl start rabbitmq_exporter.service; 
		while [ $(systemctl is-active rabbitmq_exporter) != "active" ]; do
		    echo "Starting RabbitMQ exproter again"
			sleep 20
			systemctl start rabbitmq_exporter.service; 	
		done

		echo "started with openstack"
fi

echo "  ┌────────────────────────────────────────────────────────────────┬─────────────────┐"
echo "  │   ┬ ┬ ┬ ┬ ┬ ┬    ╔══╗  ╦   ╦  ╔══╗  ╦  ╦    ╔══╗     ┌┬┐ ┌─┐   │                 │"
echo "  │   │││ │││ │││    ║═╣   ╚╗ ╔╝  ║  ║  ║  ║    ╠══╣      ││ ├┤    │   evoila GmbH   │"
echo "  │   └┴┘ └┴┘ └┴┘ o  ╚══╝   ╚═╝   ╚══╝  ╩  ╩══╝ ╩  ╩  o  ─┴┘ └─┘   │ Mainz / Germany │"
echo "  │                                                                │                 │"
echo "  │    ┌─┐┌─┐┬─┐┬  ┬┬┌─┐┌─┐  ┬┌─┐  ┌─┐┌┬┐┌─┐┬─┐┌┬┐┬┌┐┌┌─┐          │  www.evoila.de  │"
echo "  │    └─┐├┤ ├┬┘└┐┌┘││  ├┤   │└─┐  └─┐ │ ├─┤├┬┘ │ │││││ ┬          │ info@evoila.de  │"
echo "  │    └─┘└─┘┴└─ └┘ ┴└─┘└─┘  ┴└─┘  └─┘ ┴ ┴ ┴┴└─ ┴ ┴┘└┘└─┘  o o o   │                 │"
echo "  └────────────────────────────────────────────────────────────────┴─────────────────┘"

# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_START
 echo "started with docker"
fi

