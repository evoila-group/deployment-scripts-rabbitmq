#!/bin/bash
echo
echo "### Starting configuration of rabbitmq-server ... ###"
echo
#starts with parameters for dbname, user and password set in rabbitmq-template.sh
echo "rabbitmq_default_vhost = ${RABBITMQ_DEFAULT_VHOST}"
echo "rabbitmq_default_user = ${RABBITMQ_DEFAULT_USER}"

HOSTNAME=`cat /etc/hostname`

echo "RABBITMQ_BASE=$DATA_PATH
RABBITMQ_MNESIA_BASE=$DATA_PATH
RABBITMQ_MNESIA_DIR=$DATA_PATH/db
RABBITMQ_PLUGINS_EXPAND_DIR=$DATA_PATH/plugins
RABBITMQ_NODENAME=rabbit@$HOSTNAME
RABBITMQ_PID_FILE=/var/run/rabbitmq/pid" >> /etc/rabbitmq/rabbitmq-env.conf

systemctl restart rabbitmq-server

sleep 15

# overwrites rabbitmq-env.conf for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
  sed -i "s|^RABBITMQ_PID_FILE=/var/run/rabbitmq/.*$|RABBITMQ_PID_FILE=/var/run/rabbitmq/pid|" "/etc/rabbitmq/rabbitmq-env.conf"

  rabbitmqctl start_app

  sleep 15

  ls -al /var/lib/rabbitmq/
  chown -R rabbitmq:rabbitmq /var/lib/rabbitmq/.erlang.cookie
fi

rabbitmq-plugins enable rabbitmq_management

#adds new user with admin tag, sets password, sets new vhost and deletes default admin and vhost
rabbitmqctl add_user ${RABBITMQ_DEFAULT_USER} ${RABBITMQ_DEFAULT_PASS}
rabbitmqctl set_user_tags ${RABBITMQ_DEFAULT_USER} administrator
rabbitmqctl add_vhost ${RABBITMQ_DEFAULT_VHOST}
rabbitmqctl set_permissions -p ${RABBITMQ_DEFAULT_VHOST} ${RABBITMQ_DEFAULT_USER} ".*" ".*" ".*"
rabbitmqctl delete_user guest
rabbitmqctl delete_vhost /


## Generate SystemD Script
cd /etc/systemd/system/
echo "[Unit]" > rabbitmq_exporter.service
echo "Description=RabbitMQ exporter" >> rabbitmq_exporter.service
echo "After=local-fs.target network-online.target network.target" >> rabbitmq_exporter.service
echo "Wants=local-fs.target network-online.target network.target" >> rabbitmq_exporter.service
echo "" >> rabbitmq_exporter.service
echo "[Service]" >> rabbitmq_exporter.service
echo "Environment=RABBIT_USER=$RABBITMQ_DEFAULT_USER" >> rabbitmq_exporter.service
echo "Environment=RABBIT_PASSWORD=$RABBITMQ_DEFAULT_PASS" >> rabbitmq_exporter.service
echo "ExecStart=/opt/rabbitmq_exporter" >> rabbitmq_exporter.service
echo "Type=simple" >> rabbitmq_exporter.service
echo "" >> rabbitmq_exporter.service
echo "[Install]" >> rabbitmq_exporter.service
echo "WantedBy=multi-user.target" >> rabbitmq_exporter.service

chmod 755 /etc/systemd/system/rabbitmq_exporter.service
systemctl daemon-reload
systemctl enable rabbitmq_exporter.service

# Stop RabbitMQ service
# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  $OPENSTACK_STOP
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
  $DOCKER_STOP &
fi
