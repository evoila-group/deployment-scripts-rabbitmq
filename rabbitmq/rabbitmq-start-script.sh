#!/bin/bash

export REPOSITORY_RABBITMQ="https://bitbucket.org/meshstack/deployment-scripts-rabbitmq/raw/HEAD/rabbitmq"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_RABBITMQ/rabbitmq-template.sh --no-cache
chmod +x rabbitmq-template.sh
./rabbitmq-template.sh -d evoila -u evoila -p evoila -e openstack
